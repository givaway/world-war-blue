package com.HiArc.StateEngine;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class StateEngine {
	ArrayList<State> states = new ArrayList<State>();
	State current = null;
	public StateEngine()
	{
		
	}
	
	public void addState(State e)
	{
		states.add(e);
	}
	
	public void loadState(String name)
	{
		boolean found = false;
		for(int i = 0; i < states.size(); i++)
		{
			if(states.get(i).name == name)
			{
				this.current = states.get(i);
				found = true;
				break;
			}
		}
		if(found)
		{
			this.current.Start();
		}
		
	}
	
	public void update(GameContainer gc,int dt)
	{
		if(this.current != null)
		{
			this.current.update(gc, dt);
		}
	}
	
	public void render(GameContainer gc,Graphics g)
	{
		if(this.current != null)
		{
			this.current.render(gc, g);
		}
	}
}
