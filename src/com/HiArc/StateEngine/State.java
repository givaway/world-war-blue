package com.HiArc.StateEngine;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class State {
	public String name;
	ArrayList<GameObject> objects = new ArrayList<GameObject>();
	public State(String name)
	{
		this.name = name;
	}
	
	public void Start()
	{
		for(int i = 0; i < this.objects.size(); i++)
		{
			this.objects.get(i).Start();
		}
	}
	
	public void update(GameContainer gc, int dt)
	{
		for(int i = 0; i < this.objects.size(); i++)
		{
			this.objects.get(i).update(gc,dt);
		}
	}
	
	public void render(GameContainer gc, Graphics g)
	{
		for(int i = 0; i < this.objects.size(); i++)
		{
			this.objects.get(i).render(gc,g);
		}
	}
	private boolean nameTaken(String name)
	{
		boolean taken = false;

		for(int i = 0; i < this.objects.size(); i++)
		{
			if(this.objects.get(i).name.equalsIgnoreCase(name))
			{
				taken = true;
			}
		}
		
		return taken;
	}
	public String checkName(String name)
	{

		int count = 0;
		while(this.nameTaken(count==0?name:name+count))
		{
			count++;
		}
		//System.out.println(count==0?name:name+count);
		return (count==0?name:name+count);
	}

	public void addGameObject(GameObject gameObject) {
		this.objects.add(gameObject);
		
	}
}
