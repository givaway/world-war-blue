package com.HiArc.StateEngine;

import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.HiArc.Helpers.Trans;

public class GameObject {
	State parent;
	String name;
	ArrayList<Component> components = new ArrayList<Component>();
	boolean started = false;
	public Trans Transform = new Trans();
	public GameObject(State state, String name,Component... a)
	{
		this.name = state.checkName(name);

		state.addGameObject(this);
		this.parent = state;
		for (Component item : a)
		{
			item.Transform = this.Transform;
			this.components.add(item);
			
			
		}
		this.Start();
		
	}
	public void Start()
	{
		for(int i = 0; i < this.components.size(); i++)
		{
			this.components.get(i).Start();
		}
	}
	public void update(GameContainer gc, int dt)
	{
		for(int i = 0; i < this.components.size(); i++)
		{
			this.components.get(i).Transform = this.Transform;
			
			this.components.get(i).update(gc,dt);
			this.Transform = this.components.get(i).Transform;
		}
		this.Transform.update();
	}
	
	public void render(GameContainer gc, Graphics g)
	{
		for(int i = 0; i < this.components.size(); i++)
		{
			g.pushTransform();
			g.rotate(this.Transform.Position.x, this.Transform.Position.y, (float) this.Transform.rotation);
			this.components.get(i).render(gc,g);
			g.popTransform();
		}
	}
	public void AddComponent(Component a)
	{
		this.components.add(a);
	}
	public Component GetComponent(String name)
	{
		Component ret = null;
		for(int i = 0; i < this.components.size(); i++)
		{
			if(this.components.get(i).name.equalsIgnoreCase(name))
			{
				ret = this.components.get(i);
				break;

			}
		}
		return ret;
	}
}
