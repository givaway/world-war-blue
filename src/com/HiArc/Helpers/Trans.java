package com.HiArc.Helpers;

import com.HiArc.StateEngine.GameObject;

public class Trans {
	public Vector2 Position = new Vector2(0,0);
	public double rotation = 0;
	public double z = 0;
	private GameObject following, rotateTo = null;
	public void update()
	{
		if(following != null)
		{
			this.Position = following.Transform.Position;
		}
		if(rotateTo != null)
		{
			this.rotation = -this.Position.angleBetween(rotateTo.Transform.Position);
		}
	}
	public void follow(GameObject follower)
	{
		this.following = follower;
	}
	public void rotateTowards(GameObject rotateTo)
	{
		this.rotateTo = rotateTo;
	}
	
	
}
