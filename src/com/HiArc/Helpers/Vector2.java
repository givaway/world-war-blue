package com.HiArc.Helpers;

public class Vector2 {
	public float x,y = 0;
	public Vector2(float x,float y)
	{
		this.x = x;
		this.y = y;
	}
	
	public Vector2 add(Vector2 v2)
	{
		float x = this.x + v2.x;
		float y = this.y + v2.y;
		return new Vector2(x,y);
	}
	public Vector2 add(float x, float y)
	{
		float nx = this.x + x;
		float ny = this.y + y;
		return new Vector2(nx,ny);
	}
	public Vector2 add(float a)
	{
		float x = this.x + a;
		float y = this.y + a;
		return new Vector2(x,y);
	}
	public Vector2 subtract(Vector2 v2)
	{
		float x = this.x - v2.x;
		float y = this.y - v2.y;
		return new Vector2(x,y);
	}
	public Vector2 subtract(float x, float y)
	{
		float nx = this.x - x;
		float ny = this.y - y;
		return new Vector2(nx,ny);
	}
	public Vector2 subtract(float a)
	{
		float x = this.x - a;
		float y = this.y - a;
		return new Vector2(x,y);
	}
	public Vector2 multiply(Vector2 v2)
	{
		float x = this.x * v2.x;
		float y = this.y * v2.y;
		return new Vector2(x,y);
	}
	public Vector2 multiply(float x, float y)
	{
		float nx = this.x * x;
		float ny = this.y * y;
		return new Vector2(nx,ny);
	}
	public Vector2 multiply(float a)
	{
		float x = this.x * a;
		float y = this.y * a;
		return new Vector2(x,y);
	}
	public Vector2 divide(Vector2 v2)
	{
		float x = this.x / v2.x;
		float y = this.y / v2.y;
		return new Vector2(x,y);
	}
	public Vector2 divide(float x, float y)
	{
		float nx = this.x / x;
		float ny = this.y / y;
		return new Vector2(nx,ny);
	}
	public Vector2 divide(float a)
	{
		float x = this.x / a;
		float y = this.y / a;
		return new Vector2(x,y);
	}
	
	public Vector2 slope(Vector2 v2)
	{
		if (v2 != null)
		{
	
			float x = v2.x - this.x;
			float y = v2.y - this.y;
			return new Vector2(x,y);
		}
		else
		{
			return new Vector2(0,0);
		}

	}
	public double distance(Vector2 v2)
	{
		double x = Math.abs(this.x-v2.x);
		double y = Math.abs(this.y-v2.y);
		return Math.sqrt((x * x) + (y * y));
		
	}
	public double length()
	{
		return Math.sqrt(Math.pow(this.x, 2)+Math.pow(this.y, 2));
	}
	public double dot()
	{
		return ((this.x * this.x) + (this.y*this.y));
	}
	public Vector2 flip(Vector2 v2)
	{
		return new Vector2(-this.x, -this.y);
	}
	public double angleBetween(Vector2 v2)
	{
		double angle = Math.toDegrees(Math.atan2(v2.x-this.x, v2.y - this.y));
		if(angle < 0 )
		{
			angle = 360 +angle;
		}
		return Math.abs(angle);
	}
	
}

