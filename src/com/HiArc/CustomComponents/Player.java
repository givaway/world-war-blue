package com.HiArc.CustomComponents;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

import com.HiArc.Helpers.Vector2;
import com.HiArc.StateEngine.Component;

public class Player extends Component
{
	public Player()
	{
		super();
	}
	public void update(GameContainer gc,int dt)
	{
		Input input = gc.getInput();
		this.Transform.Position = new Vector2(input.getMouseX(),input.getMouseY());
	}
	public void render(GameContainer gc, Graphics g)
	{
		g.drawRect(this.Transform.Position.x, this.Transform.Position.y, 30, 30);
	}
}
