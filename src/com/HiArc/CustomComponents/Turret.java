package com.HiArc.CustomComponents;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

import com.HiArc.StateEngine.Component;

public class Turret extends Component
{

	public Turret()
	{
		super();
	}
	public void render(GameContainer gc, Graphics g)
	{
		g.drawRect(this.Transform.Position.x, this.Transform.Position.y, 20, 20);
	}
	
}
