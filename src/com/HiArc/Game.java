package com.HiArc;

import org.newdawn.slick.*;

import com.HiArc.StateEngine.StateEngine;
public class Game extends BasicGame
{
 
  public Game()
  {
     super("World War Blue");
  }
  StateEngine se;
  @Override
  public void init(GameContainer gc) throws SlickException
  {
	  se = new StateEngine();
	  se.addState(new introScreen("intro"));
	  se.loadState("intro");
  }
 
  @Override
  public void update(GameContainer gc, int delta) throws SlickException
  {
	  se.update(gc, delta);
  }
 
  @Override
  public void render(GameContainer gc, Graphics g) throws SlickException
  {
     se.render(gc, g);
  }
 
  public static void main(String[] args) throws SlickException
  {
     AppGameContainer app = new AppGameContainer(new Game());
     //app.setTargetFrameRate(60);
     app.setDisplayMode(800, 600, false);
     app.start();
  }
}